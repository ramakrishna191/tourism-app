package com.softpeoplesolutions.tourismapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class SplashScreenActivity extends AppCompatActivity {
private TextView mTextView;
private ImageView mImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        mTextView=(TextView)findViewById(R.id.spalsh_tv) ;
        mImageView=(ImageView)findViewById(R.id.andra_image);
        Animation mAnimation= AnimationUtils.loadAnimation(getApplicationContext(),R.anim.at_ts);
        mTextView.startAnimation(mAnimation);
        mImageView.startAnimation(mAnimation);
        Splash splash=new Splash();
        splash.start();
    }
    private class Splash extends Thread{
        public void run(){
            try {
                sleep(2000);
            }catch (InterruptedException e){
                e.printStackTrace();
            }
            Intent intent=new Intent(SplashScreenActivity.this,HomeActivity.class);
            startActivity(intent);
            SplashScreenActivity.this.finish();}
    }}
