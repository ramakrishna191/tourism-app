package com.softpeoplesolutions.tourismapp;

import com.softpeoplesolutions.tourismapp.network.NetworkResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;





public class Common {
    public  static JSONArray jsonParser(NetworkResponse networkResponse){
        try {
            JSONObject result=new JSONObject(networkResponse.getResponseString());
            JSONObject responseStatus=result.getJSONObject("ResponseStatus");
            String message=responseStatus.getString("message");
            if(message.equals("sucess")){
                JSONArray data=result.getJSONArray("Data");
                return data;
            }else{

            }
        } catch (JSONException e) {
            e.printStackTrace();
       }
        return null;
    }
}
