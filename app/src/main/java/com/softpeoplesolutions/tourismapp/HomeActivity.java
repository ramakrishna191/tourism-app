package com.softpeoplesolutions.tourismapp;


import android.databinding.DataBindingUtil;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import android.widget.TextView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import com.softpeoplesolutions.tourismapp.adapter.HomeCategoryAdapter;

import com.softpeoplesolutions.tourismapp.databinding.HomeActivityBinding;
import com.softpeoplesolutions.tourismapp.model.CategoryItemsModel;
import com.softpeoplesolutions.tourismapp.model.CategoryModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity implements HomeCategoryClick {

    private RecyclerView.LayoutManager layoutManager;
    public List<CategoryItemsModel> mHomeCategoryModelList;
    private HomeCategoryAdapter mHomeCategoryAdapter;
    private AdView mAdView;
    private RecyclerView mRecyclerView;
    private TextView mtoolbar_title;
    private Toolbar mToolbar;
    NetworkOperation networkOperation;
private HomeActivityBinding homeActivityBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        homeActivityBinding=DataBindingUtil.setContentView(this,R.layout.home_activity);

        setSupportActionBar(homeActivityBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        homeActivityBinding.toolbarTitle.setText("AP&TS Tourism");

        MobileAds.initialize(this,
                                "ca-app-pub-8232718013195220~5698158860");
        /*mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
*/
        mHomeCategoryModelList= new ArrayList<>();
        mHomeCategoryAdapter=new HomeCategoryAdapter(mHomeCategoryModelList,this,this);

        layoutManager = new LinearLayoutManager(this);
        homeActivityBinding.recyclerView.setLayoutManager(layoutManager);
        homeActivityBinding.recyclerView.setAdapter(mHomeCategoryAdapter);

        networkOperation = RetrofitApiClient.getApiClient().create(NetworkOperation.class);
        Call<CategoryModel> call = networkOperation.getCategoryList();
        call.enqueue(new Callback<CategoryModel>() {
            @Override
            public void onResponse(Call<CategoryModel> call, Response<CategoryModel> response) {
                mHomeCategoryModelList.addAll(response.body().Data);
                mHomeCategoryAdapter.notifyDataSetChanged();}

                @Override
            public void onFailure(Call<CategoryModel> call, Throwable t) {

            }
        }
        );
    }

    @Override
    public void onClick(int position, View view) {
        Bundle bundle = new Bundle();
        bundle.putString("CategoryId", mHomeCategoryModelList.get(position).getCategoryId());
        bundle.putString("CategoryName", mHomeCategoryModelList.get(position).getCategoryName());
        CategoryItemsFragment categoryItemsFragment = new CategoryItemsFragment();
        categoryItemsFragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.main_view, categoryItemsFragment).addToBackStack(null).commit();
    }
}



