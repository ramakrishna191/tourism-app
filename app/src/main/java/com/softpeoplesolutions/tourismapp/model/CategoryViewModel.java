package com.softpeoplesolutions.tourismapp.model;

import android.databinding.BaseObservable;
import android.view.View;

import com.softpeoplesolutions.tourismapp.CategoryItemsClick;

/**
 * Created by AJAY KRISHNA on 26-01-2018.
 */

public class CategoryViewModel extends BaseObservable  {

    private String CategoryName;
    private String CategoryImage;
    private CategoryItemsModel categoryItemsModel;

    private  CategoryItemsClick categoryItemsClick;

    public CategoryViewModel(CategoryItemsModel categoryItemsModel, CategoryItemsClick mCategoryItemsClick) {
        this.categoryItemsModel = categoryItemsModel;
        this.categoryItemsClick=mCategoryItemsClick;
    }

    public String getCategoryName() {
        return categoryItemsModel.getCategoryName();
    }

    public String getCategoryImage() {
        return categoryItemsModel.getCategoryImage();
    }

    public void  onClickCategoryItem(View v){
        categoryItemsClick.onClick(categoryItemsModel);
    }
}
