package com.softpeoplesolutions.tourismapp.model;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.softpeoplesolutions.tourismapp.model.CategoryItemsModel;

/**
 * Created by AJAY KRISHNA on 28-12-2017.
 */

public class HomeViewModel  extends BaseObservable {
    private CategoryItemsModel mHomeCategoryModel;

    public HomeViewModel(CategoryItemsModel mHomeCategoryModel) {
        this.mHomeCategoryModel = mHomeCategoryModel;
    }
    @Bindable
    public String getCategoryName() {
        return mHomeCategoryModel.getCategoryName();
    }

    public String getCategoryImage() {
        return mHomeCategoryModel.getCategoryImage();
    }

}


