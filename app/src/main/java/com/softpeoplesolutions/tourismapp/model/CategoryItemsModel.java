package com.softpeoplesolutions.tourismapp.model;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Kamalakar B on 11/18/2017.
 */

public class CategoryItemsModel implements Parcelable {
    private String CategoryId;
    private String CategoryName;
    private String CategoryImage;
    private String Description;
    private String Latitude;
    private String Longitude;
    Context ctx;

    public CategoryItemsModel (Context ctx){
        this.ctx = ctx;
    }

    public CategoryItemsModel(String categoryId, String categoryName, String categoryImage, String description, String latitude, String longitude) {
        CategoryId = categoryId;
        CategoryName = categoryName;
        CategoryImage = categoryImage;
        Description = description;
        Latitude = latitude;
        Longitude = longitude;
    }

    public static final Parcelable.Creator CREATOR = new Creator<CategoryItemsModel>() {

        @Override
        public CategoryItemsModel createFromParcel(Parcel in) {
            return new CategoryItemsModel(in);
        }

        @Override
        public CategoryItemsModel[] newArray(int size) {
            return new CategoryItemsModel[size];
        }
    };

    public CategoryItemsModel(Parcel in) {
        this.CategoryId = in.readString();
        this.CategoryName = in.readString();
        this.CategoryImage = in.readString();
        this.Description = in.readString();
        this.Latitude = in.readString();
        this.Longitude = in.readString();
    }

    public CategoryItemsModel() {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(CategoryId);
        dest.writeString(CategoryName);
        dest.writeString(CategoryImage);
        dest.writeString(Description);
        dest.writeString(Latitude);
        dest.writeString(Longitude);
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getCategoryId() {
        return CategoryId;
    }

    public void setCategoryId(String categoryId) {
        CategoryId = categoryId;
    }

    public String getCategoryName() {
        return CategoryName;
    }

    public void setCategoryName(String categoryName) {
        CategoryName = categoryName;
    }

    public String getCategoryImage() {
        return CategoryImage;
    }

    public void setCategoryImage(String categoryImage) {
        CategoryImage = categoryImage;
    }


}
