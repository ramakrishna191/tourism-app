package com.softpeoplesolutions.tourismapp.model;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.view.View;

import com.softpeoplesolutions.tourismapp.CategoryItemsClick;

/**
 * Created by AJAY KRISHNA on 06-01-2018.
 */

public class DetailViewModel extends BaseObservable {

    private String CategoryImage;
    private String Description;
    private CategoryItemsModel categoryItemsModel;


    public DetailViewModel(CategoryItemsModel categoryItemsModel) {
        this.categoryItemsModel = categoryItemsModel;
    }

    public String getCategoryImage() {return  categoryItemsModel.getCategoryImage();}

    public String getDescription() {
        return categoryItemsModel.getDescription();}
}
