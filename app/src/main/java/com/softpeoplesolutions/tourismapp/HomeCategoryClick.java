package com.softpeoplesolutions.tourismapp;

import android.view.View;

/**
 * Created by malli on 11/17/2017.
 */

public interface HomeCategoryClick {
    void onClick(int position, View view);
}
