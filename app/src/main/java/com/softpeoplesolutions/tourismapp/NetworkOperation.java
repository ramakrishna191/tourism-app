package com.softpeoplesolutions.tourismapp;

import com.softpeoplesolutions.tourismapp.model.CategoryItemModel;
import com.softpeoplesolutions.tourismapp.model.CategoryItemsModel;
import com.softpeoplesolutions.tourismapp.model.CategoryModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by AJAY KRISHNA on 12-12-2017.
 */

public interface NetworkOperation {
    @GET("getCategoriesList.php")
    Call<CategoryModel> getCategoryList();

    @Headers("Content-Type: application/json")
    @POST("categoryItems.php")
    Call<CategoryItemModel> getCategoryItems(@Body CategoryItemsModel categoryId);
}
