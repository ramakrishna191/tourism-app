package com.softpeoplesolutions.tourismapp;


import android.databinding.DataBindingUtil;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageView;

import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.gson.Gson;
import com.softpeoplesolutions.tourismapp.adapter.CategoryItemsAdopter;
import com.softpeoplesolutions.tourismapp.databinding.CategoryItemsBinding;
import com.softpeoplesolutions.tourismapp.databinding.HomeActivityBinding;
import com.softpeoplesolutions.tourismapp.model.CategoryItemModel;
import com.softpeoplesolutions.tourismapp.model.CategoryItemsModel;
import com.softpeoplesolutions.tourismapp.network.HttpAdapter;
import com.softpeoplesolutions.tourismapp.network.NetworkOperationListener;
import com.softpeoplesolutions.tourismapp.network.NetworkResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;


public class CategoryItemsFragment extends Fragment implements  CategoryItemsClick {

    private CategoryItemsAdopter mCategoryItemsAdopter;
    private List<CategoryItemsModel> mCategoryItemsList;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private Toolbar mToolbar;
    private TextView mtoolbar_title;
    NetworkOperation networkOperation;
    CategoryItemsModel categoryItemsModel;
    CategoryItemsClick mCategoryItemsClick;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        HomeActivityBinding homeActivityBinding =
                DataBindingUtil.inflate(inflater, R.layout.home_activity, container, false);
        setHasOptionsMenu(true);
        AppCompatActivity mAppCompatActivity = (AppCompatActivity) getActivity();
        mAppCompatActivity.setSupportActionBar(homeActivityBinding.toolbar);
        mAppCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        mAppCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mCategoryItemsList = new ArrayList<>();

        String categoryname = getArguments().getString("CategoryName");
        String categoryId = getArguments().getString("CategoryId");
        mCategoryItemsAdopter = new CategoryItemsAdopter(mCategoryItemsList, getContext(),this);
        mLayoutManager = new LinearLayoutManager(getActivity());

        homeActivityBinding.recyclerView.setLayoutManager(mLayoutManager);

        homeActivityBinding.recyclerView.setAdapter(mCategoryItemsAdopter);

        homeActivityBinding.toolbarTitle.setText(categoryname);

        networkOperation = RetrofitApiClient.getApiClient().create(NetworkOperation.class);

        CategoryItemsModel categoryModel = new CategoryItemsModel();
        categoryModel.setCategoryId(categoryId);
        Call<CategoryItemModel> call = networkOperation.getCategoryItems(categoryModel);
        call.enqueue(new Callback<CategoryItemModel>() {
            @Override
            public void onResponse(Call<CategoryItemModel> call, Response<CategoryItemModel> response) {
                mCategoryItemsList.clear();
                mCategoryItemsList.addAll(response.body().Data);
                mCategoryItemsAdopter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<CategoryItemModel> call, Throwable t) {
                Log.d("Response", "Failed to Connect Please Try again....");
            }
        });
        return homeActivityBinding.getRoot();
    }
    @Override
    public void onClick(CategoryItemsModel model) {

        Bundle bundle =new Bundle();
        bundle.putParcelable("CategoryItems",model);
       /* bundle.putString("transitionKey", ViewCompat.getTransitionName(sharedImageView));*/
        CategoryDetailsFragment categoryDetailsFragment = new CategoryDetailsFragment();
        categoryDetailsFragment.setArguments(bundle);
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                /*.addSharedElement(sharedImageView, ViewCompat.getTransitionName(sharedImageView))*/
                .replace(R.id.main_view, categoryDetailsFragment).addToBackStack(null).commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            getActivity().getSupportFragmentManager().popBackStack();
        }
        return super.onOptionsItemSelected(item);
    }
}
