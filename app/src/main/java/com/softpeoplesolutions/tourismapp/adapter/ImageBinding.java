package com.softpeoplesolutions.tourismapp.adapter;

import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

/**
 * Created by AJAY KRISHNA on 05-01-2018.
 */

public class ImageBinding {

    @BindingAdapter("imageurl")
    public static void homeImage(ImageView imageView,String url){
        Picasso.with(imageView.getContext())
                .load(url)
                .into(imageView);
    }

}
