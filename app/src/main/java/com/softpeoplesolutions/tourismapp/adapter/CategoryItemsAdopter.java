package com.softpeoplesolutions.tourismapp.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.softpeoplesolutions.tourismapp.CategoryItemsClick;
import com.softpeoplesolutions.tourismapp.databinding.CategoryItemsBinding;
import com.softpeoplesolutions.tourismapp.model.CategoryItemsModel;
import com.softpeoplesolutions.tourismapp.R;
import com.softpeoplesolutions.tourismapp.model.CategoryViewModel;


import java.util.List;

public class CategoryItemsAdopter extends RecyclerView.Adapter<CategoryItemsAdopter.CategoryItemViewHolder> {
    private List<CategoryItemsModel> mCategoryItemsList;
    private Context mContext;
    private CategoryItemsClick mCategoryItemsClick;

    public CategoryItemsAdopter(List<CategoryItemsModel> mCategoryItemsList, Context mContext, CategoryItemsClick mCategoryItemsClick) {
        this.mCategoryItemsList = mCategoryItemsList;
        this.mContext = mContext;
        this.mCategoryItemsClick = mCategoryItemsClick;
    }

    @Override
    public CategoryItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater =
                LayoutInflater.from(parent.getContext());
        CategoryItemsBinding categoryItemsBinding = DataBindingUtil.inflate(layoutInflater, R.layout.category_items, parent, false);
        return new CategoryItemViewHolder(categoryItemsBinding);
    }

    @Override
    public void onBindViewHolder(CategoryItemViewHolder holder, int position) {


        CategoryItemsBinding binding = holder.binding;
        binding.setCategoryviewmodel(new CategoryViewModel(mCategoryItemsList.get(position), mCategoryItemsClick));
    }

    @Override
    public int getItemCount() {
        return mCategoryItemsList.size();
    }

    public class CategoryItemViewHolder extends RecyclerView.ViewHolder {
        private CategoryItemsBinding binding;

        public CategoryItemViewHolder(CategoryItemsBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
