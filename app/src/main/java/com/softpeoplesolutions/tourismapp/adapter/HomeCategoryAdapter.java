package com.softpeoplesolutions.tourismapp.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.softpeoplesolutions.tourismapp.HomeCategoryClick;
import com.softpeoplesolutions.tourismapp.model.HomeViewModel;
import com.softpeoplesolutions.tourismapp.R;
<<<<<<<HEAD

import com.softpeoplesolutions.tourismapp.databinding.CategoryItemsHomeBinding;
import com.softpeoplesolutions.tourismapp.model.CategoryItemsModel;
=======
import com.softpeoplesolutions.tourismapp.model.HomeCategoryModel;
import com.squareup.picasso.Picasso;
>>>>>>>master

import java.util.List;

public class HomeCategoryAdapter extends RecyclerView.Adapter<HomeCategoryAdapter.MyViewHolder> {

    private List<CategoryItemsModel> homeViewModelList;

    private Context mContext;
    private HomeCategoryClick mHomeCategoryClick;

    public HomeCategoryAdapter(List<CategoryItemsModel> homeViewModelList, Context mContext, HomeCategoryClick mHomeCategoryClick) {
        this.homeViewModelList = homeViewModelList;
        this.mContext = mContext;
        this.mHomeCategoryClick = mHomeCategoryClick;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater =
                LayoutInflater.from(parent.getContext());
        CategoryItemsHomeBinding
                categoryItemsHomeBinding = DataBindingUtil.inflate(layoutInflater, R.layout.category_items_home, parent, false);
        return new MyViewHolder(categoryItemsHomeBinding);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        CategoryItemsHomeBinding binding = holder.binding;
        binding.setHomeviewmodel(new HomeViewModel(homeViewModelList.get(position)));

    }

    @Override
    public int getItemCount() {
        return homeViewModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private CategoryItemsHomeBinding binding;

        public MyViewHolder(CategoryItemsHomeBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

            binding.getRoot().setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mHomeCategoryClick.onClick(getPosition(), v);
        }
    }
}
