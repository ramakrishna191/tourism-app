package com.softpeoplesolutions.tourismapp;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by AJAY KRISHNA on 12-12-2017.
 */

public class RetrofitApiClient {

    public static final String BASE_URL1 = "http://custservice.allmysons.com/CustomerServiceApp/";
    public static final String BASE_URL = "http://www.softpeoplesolutions.com/TourismApp/";
    public static Retrofit retrofit = null;
    public static Retrofit getApiClient() {

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }}
