package com.softpeoplesolutions.tourismapp.network;

import android.os.Build;
import android.support.annotation.RequiresApi;

public class HttpAdapter {
    public static final String METHOD_GET = "GET";
    public static final String METHOD_POST = "POST";
    public static final String METHOD_DELETE = "DELETE";
    public static final String METHOD_UPDATE = "UPDATE";
    public static final String METHOD_PUT = "PUT";
    public static final String BASE_URL = "http://www.softpeoplesolutions.com/TourismApp/";
    public static final String CONTENT_TYPE_APPLICATION_URL_ENCODED = "application/x-www-form-urlencoded";
    public static final String IP_ADDRESS = "http://www.softpeoplesolutions.com/TourismApp/";
    public static final String HEADER_CONTENT_TYPE = "Content-Type";
    public static final String CONTENT_TYPE_APPLICATION_JSON = "application/json";

    public static final String GETITEMS = IP_ADDRESS + "categoryItems.php";

    public static final String CATEGORY_ITEMS = BASE_URL + "getCategoriesList.php";

    @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
    public static void getItems(NetworkOperationListener listener, String tag, String jsonData) {
        NetworkOperationn operation = new NetworkOperationn(listener, tag);
        operation.setContentType(CONTENT_TYPE_APPLICATION_JSON);
        operation.execute(GETITEMS, METHOD_POST, jsonData);
    }

    @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
    public static void getCategoryItems(NetworkOperationListener listener, String tag){
       NetworkOperationn networkOperationn = new NetworkOperationn(listener,tag);
       networkOperationn.setContentType(CONTENT_TYPE_APPLICATION_JSON);
       networkOperationn.execute(CATEGORY_ITEMS,METHOD_GET);
    }

}