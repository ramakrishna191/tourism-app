package com.softpeoplesolutions.tourismapp.network;

public interface NetworkOperationListener {
	public void operationCompleted(NetworkResponse response);

}
