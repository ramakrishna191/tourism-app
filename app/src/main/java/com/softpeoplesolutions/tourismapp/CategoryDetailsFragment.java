package com.softpeoplesolutions.tourismapp;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.location.Address;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.transition.TransitionInflater;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.softpeoplesolutions.tourismapp.databinding.CategoryDetailsFragmentBinding;
import com.softpeoplesolutions.tourismapp.model.CategoryItemsModel;
import com.softpeoplesolutions.tourismapp.model.DetailViewModel;
import com.squareup.picasso.Picasso;


public class CategoryDetailsFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMapClickListener {

    private GoogleMap mMap;
    private CategoryItemsModel categoryItemsModel;
    private Location location;
    private double lat;
    private double lng;
    private TextView category_items_tv, description, getdirections;
    private ImageView detail_category_items_img;
    private Toolbar mToolbar;
    private TextView mtoolBar;
    private DetailViewModel mDetailViewModel;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        CategoryDetailsFragmentBinding binding= DataBindingUtil.
                inflate(inflater,R.layout.category_details_fragment,container,false);
        View view = binding.getRoot();
       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setSharedElementEnterTransition(TransitionInflater.from(getContext()).inflateTransition(android.R.transition.move));
        }*/
        categoryItemsModel = getArguments().getParcelable("CategoryItems");
        mDetailViewModel=new DetailViewModel(categoryItemsModel);
        binding.setDetailsmodel(mDetailViewModel);

        AppCompatActivity mAppCompatActivity = (AppCompatActivity) getActivity();
        mAppCompatActivity.setSupportActionBar(binding.toolbar);
        mAppCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        mAppCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setHasOptionsMenu(true);
        binding.toolbar.setTitle(categoryItemsModel.getCategoryName());

        String latitude = categoryItemsModel.getLatitude();
        String longitude = categoryItemsModel.getLongitude();
        lat = Double.parseDouble(latitude);
        lng = Double.parseDouble(longitude);


        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mToolbar.setTitle(categoryItemsModel.getCategoryName());
        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng location = new LatLng(Double.parseDouble(categoryItemsModel.getLatitude()), Double.parseDouble(categoryItemsModel.getLongitude()));
        mMap.addMarker(new MarkerOptions().position(location).title(""));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(location));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(21));
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(location)      // Sets the center of the map to Mountain View
                .zoom(17)                   // Sets the zoom
                .bearing(0)                // Sets the orientation of the camera to east
                .tilt(0)                   // Sets the tilt of the camera to 30 degrees
                .build();                   // Creates a CameraPosition from the builder
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        mMap.setOnMapClickListener(CategoryDetailsFragment.this);
    }

    @Override
    public void onMapClick(LatLng latLng) {
        Bundle bundle = new Bundle();
        bundle.putDouble("lattitude", lat);
        bundle.putDouble("longitude", lng);
        GoogleMapsFragment googleMapsFragment = new GoogleMapsFragment();
        googleMapsFragment.setArguments(bundle);
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.main_view, googleMapsFragment).addToBackStack(null).commit();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            getActivity().getSupportFragmentManager().popBackStack();
        }
        return super.onOptionsItemSelected(item);

    }
}
