package com.softpeoplesolutions.tourismapp;

import android.view.View;
import android.widget.ImageView;

import com.softpeoplesolutions.tourismapp.model.CategoryItemsModel;


public interface CategoryItemsClick {
    void onClick(CategoryItemsModel model);
}
